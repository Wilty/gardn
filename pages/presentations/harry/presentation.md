---
title: harry
media_order: 'harry2-11-08-19.jpg,harry-11-08-19.jpg,harry1.JPG,harry.JPG'
date: '14:49 11-08-2019'
hide_git_sync_repo_link: false
hide_title: false
---

![](harry2-11-08-19.jpg)

---
![](harry-11-08-19.jpg)

---
![](harry1.JPG)

---
![](harry.JPG)

---
# The Australian magpie (Gymnorhina tibicen) 
Is a medium-sized black and white passerine bird native to Australia and southern New Guinea. Although once considered to be three separate species, it is now considered to be one, with nine recognised subspecies. A member of the Artamidae, the Australian magpie is placed in its own genus Gymnorhina and is most closely related to the black butcherbird (Melloria quoyi). It is not, however, closely related to the European magpie, which is a corvid. [Wiki](https://en.wikipedia.org/wiki/Australian_magpie)

_**Harry will occasionally swope a delivery person but is happy for me and some of the neighbours to get close, he and his tribe often follow me around the yard looking for bugs I may unearth.**_