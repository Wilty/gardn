---
title: 'Haier HBM450WH1 450L gallery'
media_order: 'damaged-seal.JPG,haier-450l-temperature-panel.JPG,haier-450l-inside-shelf.JPG,Haier-HBM450WH1-450L-Bottom-Mount-Fridge-Door-Open-high.jpeg'
date: '10:52 11-08-2019'
hide_git_sync_repo_link: false
hide_title: false
---

# Wear and Tear

### From the drawer when door isn't fully opened

![](damaged-seal.JPG)

---

# Temperature 

Temperature control and thermometer
![](haier-450l-temperature-panel.JPG)

---

# Shelving and light

Sturdy and can take a bit of weight (20l of soy milk)
![](haier-450l-inside-shelf.JPG)

---

# New

![](Haier-HBM450WH1-450L-Bottom-Mount-Fridge-Door-Open-high.jpeg)