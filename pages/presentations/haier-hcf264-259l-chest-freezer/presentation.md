---
title: ' Haier HCF264 259L Chest Freezer '
media_order: 'Haier-HCF264-258L-Chest-Freezer-Hero-high.jpeg,Haier-HCF264-258L-Chest-Freezer-Door-Open.jpeg,Haier-HCF264-258L-Chest-Freezer-ice-build-up.jpg'
date: '14:00 11-08-2019'
hide_git_sync_repo_link: false
hide_title: false
---

# Haier HCF264

![](Haier-HCF264-258L-Chest-Freezer-Hero-high.jpeg)

---

# Door Open

![](Haier-HCF264-258L-Chest-Freezer-Door-Open.jpeg)

---

# Ice Build Up

![](Haier-HCF264-258L-Chest-Freezer-ice-build-up.jpg)