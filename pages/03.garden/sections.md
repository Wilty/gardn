---
title: 'In The Garden'
media_order: IMG_6017.jpg
published: true
publish_date: '07-08-2006 22:09'
hide_git_sync_repo_link: false
hide_page_title: true
content:
    items: '@self.children'
    order:
        by: folder
        dir: asc
    taxonomy:
        tag:
            - view1
            - view2
hide_next_prev_page_buttons: false
---

Gardening in Australia has changed dramatically in the last decade due to prolonged drought, most of us have had to rethink how we garden as a consequence. Drought tolerant plants once rarely thought of are now the principle concept behind not just new gardens but also many established gardens as even the average quarter acre block can consume many litres of water if you stick with traditional European or more specifically the English style of gardening that was once so common. However this doesn't mean we should stay within xeriscaping but how we plant and irrigate needs more consideration then we once gave it. 

Today awareness of waterwise plants has grown considerably as has the importance of mulching and adding as much organic matter into the soil as possible. This helps to retain moisture but if you have a heavy clay soil, gypsum should be added or a thicker layer of mulch to help with water penetration, without it much will be lost due to run off and evaporation. Organic matter also makes a plant stronger and healthier thus in periods of stress you'll have a far greater survival rate.

Although organic matter in and of itself doesn't directly fed the plants it improves soil structure and promotes good micro organism that will help abate disease within the soil and once they die release useful nutrients that plants can then feed from.

One the following pages you will see how members of the forum have setup their gardens along with a number of short articles on botanic gardens and notable Aussie gardens which will be added over time. Feel free to add content or even edit this page :) 