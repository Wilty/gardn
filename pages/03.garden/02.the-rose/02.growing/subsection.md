---
title: 'Growing Roses'
taxonomy:
    tag:
        - view1
hide_git_sync_repo_link: false
hide_page_title: false
---

It has been said by many-that roses are 'gross feeders'. But in the same way that overfeeding yourself, your children or your pets can be detrimental to their health the same can hold true for roses. Overfed roses are more susceptible to wind, sun and frost damage because of the soft, sappy growth overfeeding produces. Aphids which are generally a result of too much nitrogen and other pests also find this fragile growth attractive and can weaken the rose and render it more susceptible to disease. Roses need 'Tough Love' to live long and fruitful lives. Plenty of organic matter is always the way to go.

**Sunlight**

Most rose books are written in the Northern Hemisphere where the conditions are very different to those in most of Australia. When these books say that roses need 6 to 8 hours of sunshine a day, this applies to the conditions found there where the sun is not as harsh. The summer sunshine is milder,and the shade denser in the Northern Hemisphere than in almost all areas of Australia.

In most parts of Australia our summer sun is so fierce that it burns the roses and in some areas will shock them into survival type dormancy (which can also be beneficial).. However some modern roses may not survive these conditions. In these areas,3 to 5 hours of morning sunshine is more than adequate for your roses. The roses need shelter from afternoon sun which can be supplied by trees, shrubs, walls or shadecloth.

**Wind**

Wind causes a lot of damage to roses-not only with 'wind rock' but due to the dehydration the hot summer winds can cause. It is now generally recognised that planting your roses with the graft a few inches beneath the surface gives added stability to the plant and also has the benefit of the grafted cultivar being able to put down roots of its own. Your roses need protection from the drying winds, and tall shrubs or walls on the windward side will go a long way towards supplying this.
