---
title: 'The Roses'
taxonomy:
    tag:
        - view1
hide_git_sync_repo_link: false
hide_page_title: false
---

Species roses are the original wild roses. Rose fossils have been found that are millions of years old, so it's difficult to know how many of the original 200 or so discovered species roses remain in existence because of natural hybridization but there are still many growing in gardens all over the world. The species roses have travelled a long way from the wild places, been discovered and rediscovered and many lost along the way. There are records to show they were cultivated by the Egyptians, Turks, Greeks and Romans, by Chinese emperors and English Kings, and Cherokee Indians… There are references to the rose in ancient Sanskrit literature in an ancient Samarian cuneiform tablet …

Species and hybrid species roses have wonderfully perfumed flowers, aromatic foliage in both evergreen and autumn colours as well as a variety of colourful hips in interesting shapes which can bring ornamental delight to the winter garden. Some make small bushes; others are huge ramblers. The more thorny or prickly varieties can look quite formidable but the thorns come in so many different sizes, shapes and colours they add their own interest. Blooms are borne singly or in clusters and most have only 5 petals which ensure a look of simple, uncomplicated charm. One exception is R. sericea with only four petals. Most species roses are pink, yellow, red or white and the blooming period is usually short but there are some remontant varieties that repeat in autumn. Foliage may be rough to the touch or as light and delicate as a fern.

Leaflets can number from 3 to 15, be tiny or large, shiny or matt, hairy or smooth, green, bluish green, grey green, purple green, red green etc. Foliage can resemble ferns and other plant types. Most flower only once a year but a few repeat in Autumn but Chinas (Teas) flower most of the year. Species roses fall into a further 5 categories: Cinnamomeae, Caninae, Synstylae, Pimpinellifoliae and Carolinae. Many of us have probably never seen a wild rose except for a few that are still popular and can be found in many nurseries. Rosa banksia (4 varieties), laevigata, bracteata, moschata and other ‘musk’ roses are some example of these

Their descendants however found their way into the hearts of gardeners on every continent. These are the gallicas, damasks, albas, moss, centifolias, portlands, chinas, teas, bourbons, noisettes and hybrid perpetuals. 