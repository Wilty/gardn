---
title: Evolution
media_order: macie-jones-UqVmpW00GuE-unsplash1.jpg
taxonomy:
    tag:
        - view1
hide_git_sync_repo_link: false
hide_page_title: false
content:
    items: '- ''@self.children'''
    order:
        by: date
        dir: desc
    limit: '5'
    pagination: '1'
    url_taxonomy_filters: '1'
hide_next_prev_page_buttons: '0'
hero_classes: ''
hero_image: macie-jones-UqVmpW00GuE-unsplash1.jpg
header_image_alt_text: ''
header_image_credit: ''
header_image_creditlink: ''
blog_url: /blog
show_sidebar: '1'
show_breadcrumbs: '1'
show_pagination: '1'
bricklayer_layout: '1'
display_post_summary:
    enabled: '0'
post_icon: ''
feed:
    limit: '10'
    description: ''
---

The first species roses originated in central Asia. They spread out by colonisation and seed dispersal until they covered most parts of the Northern Hemisphere. The further they spread the more they evolved. Cytologists and geneticists have found that the oldest existing species roses have 7 chromasomes in their gametic cells and 14 chromasomes in their somatic cells. Having 7 chromasomes makes them haploid. The further these roses spread throughout the Northern Hemisphere the more changes they underwent in their chromasome counts. They first became diploid-that is they doubled their original 7 chromasomes to 14.

Each evolution added 7 more chromasomes to the original count so we have

HAPLOID (single) =7 chromasomes

DIPLOID (double) =14 chromasomes

TRIPLOID (triple) =21 chromasomes

TETRAPLOID(fourfold) =28 chromasomes

PENTAPLOID (fivefold) =35 chromasomes

HEXAPLOID (sixfold) =42 chromasomes

OCTOPLOID (eightfold) =56 chromasomes


Any increase from the basic chromasome count of 7 is known as POLYPLOIDY. Polyploidy had a practical aspect in that it enabled these roses to extend their area of habitation and to adapt to new conditions such as soil and climate.

There are few polyploids in the birthplace of roses but as they spread both west and north this polyploidy increases until it reaches the octoploid stage inR. acicularis which inhabits the most northerly areas. This octoploid state enabled R. acicularis to adapt to and colonise the arctic region.

Species roses fall into five distinct groups: Cinnamomeae, Caninae, Synstylae, Pimpinellifoliae and Carolinae. Almost all the species roses fall into one of these categories.

**Cinnamomeae **(Cassiorhodon)

_Rosa acicularis nipponensis, R. amblyotis, R. arkansana, R. banksiopsis, R. beggeriana, R. bella, R. blanda, R. californica, R. californica plena, R. caudata, R. coriifolia, R. corymbulosa, R. davidii, R. davurica R. fargesii, R. farreri persetosa, R. fedtschenkoana, R. forrestiana, R. gymnocarpa, R. hemsleyana, R. bolodonta, R. latibracteata, R. macrophylla, R. majalis, R. marretii, R. maximowicziana, R. melina, R. micrugosa, R. mobavensis, R. moyesii, R. multibracteata, R. muriela, R. nanothamnus, R. nutkana, R. paulii, R. pendulina, R. pisocarpa, R. prattii, R. pyrifera, R. rugosa, plus R. rugosa alba,rubra and typica, R. sertata, R. setipoda, R. spaldingii, R. suffulta, R. sweginzowii macrocarpa, R. ultramontana, R. wardii, R. webbiana, R. willmottiae, R. woodsii, R.woodsii fendleri, R. yainacensis._

**Caninae**

_R. agrestis, R. biebersteinii, R. britzensis, R. canina, R. collina R. corymbifera, R. dumales, R. eglanteria, R. glauca, R. inodora, R. jundzillii, R.micrantha, R. mollis, R. orientalis, R. pulverulenta, R. serafinii, R. sherardii, R. sicular, R. stylosa, R.tomentosa, R. villosa._

**Synstylae** (Synstylae means that the styles are fused into a single column-these roses can also be considered the ‘musk roses’.)

_R. anemoneflora, R.arvensis, R. brunonii, R. filipes, R. gentiliana, R. helenae, R. henryi, R. longicuspis, R. luciae, R. moschata, R. moschata nastarana, R. mulliganii,R. multiflora, R. multiflora carnea, R. multiflora cathayensis, R. multiflora grevillii, R. multiflora watsoniana, R. multiflora wilsonii, R. phoenicia, R. sempervirens, R. setigera, R. sinowilsonii, R. soulieana, R. wichuraiana._

**Pimpinellifoliae**

_R. dunwichensis, R. ecae, R. foetida syn R. lutea, R. foetida bicolour, R. foetida persiana, R. hugonis, R. koreana, R. pimpinellifolia syn R. spinosissima, R. primula, R. sericea, R. xanthina._

**Carolinae**

_R. palustris, R. virginiana, R. virginiana plena, R. carolina, R. carolina grandiflora, R. carolina plena, R. nitida, R. foliolosa._ 
