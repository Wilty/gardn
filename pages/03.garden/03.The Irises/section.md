---
title: 'The Irises'
taxonomy:
    tag:
        - view2
        - view1
hide_git_sync_repo_link: false
hide_page_title: false
---

The botanical division of the iris genus into divisions and subdivisions is an old one, and does not necessarily reflect the closeness or distance of plants according to their DNA, but simplifies the groupings somewhat for the grower and collector.  The first grouping gives us the Rhizomatous-rooted Division, Tuberous-rooted division and Bulbous division all of which are self explanatory. The rhizomatous-rooted group is then further divided to form the 3 subdivisions Apogon, Evansia, and Pogon. Apogon means lacking beards. Evansia iris have a crest or ridge in place of a beard, and Pogon irises have a beard. While all these species are all native to the Northern Hemisphere, they are all grown to a greater or lesser extent by gardeners and collectors in Australia. In this work I have attempted to provide concise notes on each grouping, followed by cultivation advice for the more commonly grown species and hybrids.   

The species of the genus iris number in the hundreds, and many have hybridised in the wild so that classification has not always been straight forward. Species of iris range from the tall, elegant Japanese irises with dinner plate blooms, to the tiniest of bearded irises _I.attica_ with it's delicate short lived flowers. The vary in cultural requirements from the Louisianas that love to live in wet and humid conditions all year, to the bulbous juno irises that need very sharp drainage. Rather than attempt to deal with each individual species, I have undertaken to give you an overview of the various groups of species and hybrids.




