---
title: Sidebar
hide_git_sync_repo_link: false
routable: false
visible: false
position: top
---

**Remember** when _350_
was the tipping point **??**
Humans with foolish hope
or is it willful ignorance?
