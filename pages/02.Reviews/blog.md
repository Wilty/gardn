---
title: 'My Reviews '
media_order: vincent-botta-wYD_wfifJVs-unsplash1.jpg
hide_git_sync_repo_link: false
hero_classes: 'text-light title-h1h2 overlay-dark-gradient hero-large parallax'
hero_image: floppy-disks-by-brett-jordan-MFLNpz5FZRk-unsplash.jpg
blog_url: /reviews
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
bricklayer_layout: true
display_post_summary:
    enabled: false
feed:
    limit: 10
---

### **These reviews are due to my ever** ***diminishing storage*** CAPACITY