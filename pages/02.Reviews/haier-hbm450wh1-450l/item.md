---
title: 'Haier HBM450WH1 450L'
media_order: 'Haier-HBM450WH1-450L-Bottom-Mount-Fridge-Door-Open-high.jpeg,damaged-seal.JPG'
published: true
date: '00:52 11-08-2019'
taxonomy:
    category:
        - reviews
    tag:
        - appliances
        - review
hide_git_sync_repo_link: false
hero_image: Haier-HBM450WH1-450L-Bottom-Mount-Fridge-Door-Open-high.jpeg
header_image_alt_text: 'Haier Fridge/Freezer White'
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

- **Bought On: 01 Jun, 2017**
- **Price: $745**
- **From: [Appliances Online](https://www.appliancesonline.com.au/)** (via eBay sale)
- **Recommend: Yes** (caveats)
- **Rating: 4 out of 5**

===

So we've had it a little over 2 years now and honestly its a fantastic fridge, the 3 seperate temp zones are great but it has some issues. 

The bloody drawers stick especially the vegetable draw you really have to make a conscious effort to use both hands at all times, otherwise it's going to jam. But all the drawers are problematic just to a lesser extent compared to the vegetable compartment. The freezer drawers are too small they don't have enough height. Having a couple of shelves instead of drawers in the freezer would've been a better option. The meat drawer has roughed up - gouged the door seal but nothing terrible after 2 years of use (dog hair for scale)

[presentation="presentations/haier-hbm450wh1-450l-gallery"]


Back to the postive, the fridge interior light is fantastic, and if the door is left open for to long it beeps to let you know how much of a numpty you are and the older I get the more often I leave it open :( 

It uses very little power the most I've seen it draw is 40w and our electricity bill has dropped about $100 a quarter, as our previous fridge was old - close to 30 when we finally retired it, so the Haier has paid for itself. We also have far less spoilage due to the better temp control and having the bottom drawer in the fridge set to 0 means the meat stays fresher far longer and it's great for a really cold drink. 

The caveat if I could find a similar sized fridge with the same power usage and within $50 I'd buy it as the drawers really are that annoying at times.

