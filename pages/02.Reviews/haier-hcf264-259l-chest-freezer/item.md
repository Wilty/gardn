---
title: 'Haier HCF264 259L'
media_order: Haier-HCF264-258L-Chest-Freezer-Temperature-Control-high.jpeg
date: '13:52 11-08-2019'
taxonomy:
    category:
        - reviews
    tag:
        - appliances
        - 'chest freezer'
        - freezer
        - 'white good'
hide_git_sync_repo_link: false
hero_image: Haier-HCF264-258L-Chest-Freezer-Temperature-Control-high.jpeg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

-   **Bought On: 22 July, 2019**
-   **Price: $516**
-   **From: [Appliances Online ](https://www.appliancesonline.com.au/product/haier-hcf264-258l-chest-freezer)**
-   **Recommend: Not yet**
-   **Rating: 3.5 out of 5**

===

Haven't owned it long enough to say if this is a good freezer but one downside already a lot of frost/ice build up after just 2 weeks and it should've come with more then 1 basket. However it's a good size and well it's a freezer and it does freeze stuff :)

[presentation="presentations/haier-hcf264-259l-chest-freezer"]