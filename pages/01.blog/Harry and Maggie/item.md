---
title: 'Harry and Maggie'
media_order: harry1.JPG
date: '17:34 08/10/2018'
taxonomy:
    category:
        - blog
    tag:
        - photography
        - birds
hide_git_sync_repo_link: false
hero_classes: 'text-dark title-h1h2 overlay-light hero-large parallax'
hero_image: harry1.JPG
header_image_alt_text: 'Harry the Magpie'
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
subtitle: 'finding beauty in the burbs'
---

We now feed and water Harry, Maggie and their ever changing family as the idea of not feeding the wildlife is kind of absurd in our current climate of destruction. I once thought we shouldn't but I also once thought we should grow indigenous plants but we actually just need to grow whatever grows and grows well to reduce our carbon footprint.  

A picture of Harry :)  
[presentation="presentations/harry"]

Harry's nemesis
[presentation="presentations/wattlebird"]
